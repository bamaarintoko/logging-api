<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 3/13/2019
 * Time: 5:57 PM
 * @param string $title_
 * @param string $content_
 * @param array $player_id
 * @return mixed
 */

function fail_message()
{
    return $data = array("status" => false,
        "message" => "cek your parameter",
        "kode" => 401,
        "result" => []);
}

function message($status = true, $message = 'message', $result = [], $kode = 200)
{
    return $data = array("status" => $status,
        "message" => $message,
        "kode" => $kode,
        "result" => $result);
}
function send_notif($title_ = "title", $content_ = "content", $player_id = [])
{
    $title   = array("en" => $title_);
    $content = array("en" => $content_);

    $fields = array('app_id' => "232a46ae-3638-46e4-b84a-0cd5be159e6e",
        'include_player_ids' => $player_id,
        'data' => array("foo" => "bar"),
        'contents' => $content,
        'headings' => $title);

    $fields = json_encode($fields);
//    print("\nJSON sent:\n");
//    print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

//    return $response;
}