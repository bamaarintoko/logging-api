<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\AuthController@register');
Route::get('download', 'API\TpkController@download');
Route::get('cek', 'API\SettingController@cek');
Route::group(['middleware' => ['auth:api',
    'check']], function () {
    Route::post('add_raw_stock', 'API\TpkController@insert_raw_stock');
    Route::post('notif', 'API\TpkController@notif');
    Route::post('add_kategori', 'API\KategoriController@add_kategori');
    Route::get('get_kategori', 'API\KategoriController@get_kategori');
    Route::get('get_raw_stock', 'API\TpkController@get_raw_stock');
    Route::get('get_total_raw_stock', 'API\TpkController@get_total_raw_stock');
    Route::get('get_paid_stock', 'API\TpkController@get_paid_stock');
    Route::post('insert_paid_stock', 'API\TpkController@insert_paid_stock');
    Route::post('insert_out_stock', 'API\TpkController@insert_out_stock');
    Route::get('get_jenis_kayu', 'API\TpkController@get_jenis_kayu');
    Route::get('get_jenis_raw_stock', 'API\TpkController@get_jenis_kayu_by_raw_stock');
    Route::get('get_jenis_paid_stock', 'API\TpkController@get_jenis_kayu_by_paid_stock');
    Route::get('get_stock_kayu', 'API\TpkController@get_stock_kayu');
    Route::post('create_surat_jalan', 'API\TpkController@create_surat_jalan');
    Route::get('cek_surat_jalan', 'API\TpkController@cek_surat_jalan');
    Route::get('detail_tpk_surat_jalan', 'API\TpkController@detail_tpk_surat_jalan');
    Route::post('create_out_item', 'API\TpkController@create_out_item');
    Route::post('destroy_surat_jalan', 'API\TpkController@destroy_surat_jalan');
    Route::post('destroy_item', 'API\TpkController@destroy_item');
    Route::post('update_surat_jalan', 'API\TpkController@update_surat_jalan');
    Route::post('upload', 'API\TpkController@upload');
    Route::post('delete', 'API\TpkController@delete');
    Route::get('get_shipping', 'API\TpkController@get_shipping');
    //    LOGPOND
    Route::get('get_logpond_shipping', 'API\LogpondController@get_logpond_shipping');
    Route::get('get_logpond_out_shipping', 'API\LogpondController@get_logpond_out_shipping');
    Route::get('get_detail', 'API\LogpondController@get_detail');
    Route::get('get_detail_shipping', 'API\LogpondController@get_detail_shipping');
    Route::post('update_shipping', 'API\LogpondController@update_shipping');
    Route::get('get_logpond', 'API\LogpondController@get_logpond');
    Route::get('cek_log_letter', 'API\LogpondController@cek_log_letter');
    Route::post('log_letter', 'API\LogpondController@log_letter');
    Route::post('destroy_log_letter', 'API\LogpondController@destroy_log_letter');
    Route::get('log_pond_attr', 'API\LogpondController@log_pond_attr');
    Route::post('out_log_item', 'API\LogpondController@out_log_item');
    Route::post('update_log_surat_jalan', 'API\LogpondController@update_log_surat_jalan');
    Route::post('delete_log_item', 'API\LogpondController@delete_log_item');
    //    KASIR
    Route::post('update_tarif', 'API\TarifController@update_tarif');
    Route::get('get_list_tarif', 'API\TarifController@get_list_tarif');
    Route::get('get_detail_pembayaran', 'API\TarifController@get_detail_pembayaran');
    Route::post('update_pembayaran', 'API\TarifController@update_pembayaran');
    Route::get('get_list_tarif_paid', 'API\TarifController@get_list_tarif_paid');
    Route::post('add_balance', 'API\TarifController@add_balance');
    Route::post('accept_balance', 'API\TarifController@accept_balance');
    Route::get('get_mutasi', 'API\TarifController@get_mutasi');
    Route::get('get_date', 'API\TarifController@get_date');
    Route::get('get_saldo', 'API\TarifController@get_saldo');
    Route::get('get_pending_saldo', 'API\TarifController@get_pending_saldo');
    Route::get('get_history_saldo', 'API\TarifController@get_history_saldo');
    Route::get('cron_job', 'API\TarifController@cron_job');

    //    <Inventory>
    Route::get('cek_spm', 'API\InventoriController@cek_spm');
    Route::post('create_spm', 'API\InventoriController@create_spm');
    Route::post('add_out_item', 'API\InventoriController@add_out_item');
    Route::post('delete_out_item', 'API\InventoriController@delete_out_item');
    Route::get('select_item', 'API\InventoriController@select_item');
    Route::post('get_detail_spm', 'API\InventoriController@get_detail_spm');
    Route::post('update_out_item', 'API\InventoriController@update_out_item');
    Route::post('create_pdps', 'API\InventoriController@create_pdps');
    Route::get('cek_pdps', 'API\InventoriController@cek_pdps');
    Route::post('add_in_item', 'API\InventoriController@add_in_item');
    Route::post('delete_in_item', 'API\InventoriController@delete_in_item');
    Route::post('update_pdps', 'API\InventoriController@update_pdps');
    Route::get('get_list_pdps', 'API\InventoriController@get_list_pdps');
    Route::get('get_stock', 'API\InventoriController@get_stock');
    //    <Inventory/>

});
Route::post('login', 'API\AuthController@login');
Route::get('test', 'API\AuthController@test');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
