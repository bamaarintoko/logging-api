<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $buyer_id
 * @property string $buyer_name
 * @property string $buyer_cp
 * @property string $buyer_address
 */
class Buyer extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'buyer';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'buyer_id';

    /**
     * @var array
     */
    protected $fillable = ['buyer_name', 'buyer_cp', 'buyer_address'];

}
