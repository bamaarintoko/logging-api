<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $pdps_id
 * @property int $item_id
 * @property int $quantity
 * @property int $buy_price
 * @property string $note
 * @property int $created_by
 * @property string $created_date
 */
class IncomingInventory extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'incoming_inventory';

    /**
     * @var array
     */
    protected $fillable = ['id', 'pdps_id', 'item_id', 'quantity', 'buy_price', 'note', 'created_by', 'created_date'];

}
