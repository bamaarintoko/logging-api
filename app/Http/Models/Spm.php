<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nomor_surat
 * @property string $note
 * @property string $created_date
 * @property int $created_by
 * @property string $status
 */
class Spm extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'spm';

    /**
     * @var array
     */
    protected $fillable = ['nomor_surat', 'note', 'created_date', 'created_by', 'status'];
    public function item(){
        return $this->hasMany('App\Http\Models\InventoriOut', 'spm_id', 'id')->join('master_inventory','master_inventory.id','inventory_out.item_id');
    }
}
