<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $jenis
 */
class JenisKayu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'jenis_kayu';

    /**
     * @var array
     */
    protected $fillable = ['jenis'];

    public function kategori(){
        return $this->hasMany('App\Http\Models\KategoriKayu','id_jenis_kayu','id')->select('id_jenis_kayu','id','nama_kategori');
    }

}
