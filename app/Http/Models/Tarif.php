<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $reference_number
 * @property string $origin
 * @property int $destination
 * @property string $nomor_surat
 * @property string $nomor_invoice
 * @property string $status
 * @property string $status_pengiriman
 * @property int $ammount
 * @property string $created_at
 * @property string $shipped_date
 * @property string $arrived_date
 * @property int $created_by
 * @property string $updated_at
 */
class Tarif extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tarif';

    /**
     * @var array
     */
    protected $fillable = ['reference_number',
        'origin',
        'destination',
        'nomor_surat',
        'nomor_invoice',
        'status',
        'status_pengiriman',
        'ammount',
        'created_at',
        'shipped_date',
        'arrived_date',
        'created_by',
        'updated_at'];

    public function paid_user(){
        return $this->hasOne('App\User','id',"paid_by");
    }

}
