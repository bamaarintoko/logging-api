<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $logpond_id
 * @property int $jenis_kayu
 * @property int $panjang_kayu
 * @property int $jumlah_batang
 * @property int $volume
 * @property string $catatan
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $from_order
 */
class LogpondStock extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'logpond_stock';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'logpond_id';

    /**
     * @var array
     */
    protected $fillable = ['jenis_kayu', 'panjang_kayu', 'jumlah_batang', 'volume', 'catatan', 'created_at', 'created_by', 'updated_at', 'updated_by','from_order'];

}
