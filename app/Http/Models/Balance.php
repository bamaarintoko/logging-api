<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $balance_id
 * @property int $amount
 * @property string $image
 * @property string $note
 * @property string $status
 * @property int $created_by
 * @property string $created_date
 * @property int $accepted_by
 * @property string $accepted_date
 */
class Balance extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'balance';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'balance_id';

    /**
     * @var array
     */
    protected $fillable = ['amount', 'note', 'status', 'created_by', 'created_date', 'accepted_by', 'accepted_date'];
    public function created_user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

}
