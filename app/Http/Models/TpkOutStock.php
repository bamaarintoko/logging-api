<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $reference_number
 * @property int $jenis_kayu
 * @property int $panjang_kayu
 * @property int $volume
 * @property int $jumlah_batang
 * @property int $created_by
 * @property string $created_at
 */
class TpkOutStock extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tpk_out_stock';

    /**
     * @var array
     */
    protected $fillable = ['id', 'reference_number', 'jenis_kayu', 'panjang_kayu', 'volume', 'jumlah_batang', 'created_by', 'created_at'];

}
