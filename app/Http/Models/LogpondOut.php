<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $logpond_out_id
 * @property int $reference_number
 * @property int $jenis_kayu
 * @property int $panjang_kayu
 * @property int $jumlah_batang
 * @property int $volume
 * @property string $created_at
 * @property int $created_by
 */
class LogpondOut extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'logpond_out';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'logpond_out_id';

    /**
     * @var array
     */
    protected $fillable = ['reference_number', 'jenis_kayu', 'panjang_kayu', 'jumlah_batang', 'volume', 'created_at', 'created_by'];

}
