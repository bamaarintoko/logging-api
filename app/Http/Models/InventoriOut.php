<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $spm_id
 * @property int $item_id
 * @property int $quantity
 * @property string $status
 * @property string $note
 * @property int $created_by
 * @property string $created_date
 */
class InventoriOut extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'inventory_out';

    /**
     * @var array
     */
    protected $fillable = ['id', 'spm_id', 'item_id', 'quantity', 'status', 'note', 'created_by', 'created_date'];

}
