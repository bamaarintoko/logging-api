<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $jenis_kayu
 * @property int $panjang_kayu
 * @property int $volume
 * @property int $jumlah_batang
 * @property string $kondisi
 * @property string $created_at
 * @property int $created_by
 */
class TpkRawStock extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tpk_raw_stock';

    /**
     * @var array
     */
    protected $fillable = ['jenis_kayu',
        'panjang_kayu',
        'volume',
        'jumlah_batang',
        'kondisi',
        'created_at',
        'created_by'];

    public function kategori()
    {
        return $this->hasOne('App\Http\Models\KategoriKayu', 'id', 'jenis_kayu')->select('id', 'nama_kategori');
    }

//    public function sum(){
//        return $this->hasOne('App\Http\Models\KategoriKayu', 'id', 'jenis_kayu');
//    }
}
