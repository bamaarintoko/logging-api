<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $satuan
 * @property float $harga_beli
 * @property int $created_by
 * @property string $created_date
 */
class MasterInventory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'master_inventory';

    /**
     * @var array
     */
    protected $fillable = ['id', 'kode_barang', 'nama_barang', 'satuan', 'harga_beli', 'created_by', 'created_date'];

}
