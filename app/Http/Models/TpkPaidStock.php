<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $invoice_id
 * @property int $jenis_kayu
 * @property int $panjang_kayu
 * @property int $jumlah_batang
 * @property int $volume
 * @property int $created_by
 * @property string $created_at
 */
class TpkPaidStock extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tpk_paid_stock';

    /**
     * @var array
     */
    protected $fillable = ['invoice_id', 'jenis_kayu', 'panjang_kayu', 'jumlah_batang', 'volume', 'created_by', 'created_at'];

    public function kategori()
    {
        return $this->hasOne('App\Http\Models\KategoriKayu', 'id', 'jenis_kayu')->select('id', 'nama_kategori');
    }

}
