<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $reference_number
 * @property string $destination
 * @property string $dest_name
 * @property string $driver_name
 * @property string $police_number
 * @property string $phone_number
 * @property string $out_status
 * @property string $process_date
 * @property string $shipping_date
 * @property string $arrived_date
 * @property string $created_at
 * @property int $created_by
 * @property int $accepted_by
 * @property int $buyer_id
 * @property string $file
 */
class TpkDeliveryOrder extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tpk_delivery_order';

    /**
     * @var array
     */
    protected $fillable = ['reference_number', 'destination', 'dest_name', 'driver_name', 'police_number', 'phone_number', 'out_status', 'process_date', 'shipping_date', 'arrived_date', 'created_at', 'created_by','accepted_by','buyer_id','file'];

    public function out_item()
    {
        return $this->hasMany('App\Http\Models\TpkOutStock', 'reference_number', 'id')->join('kategori_kayu','kategori_kayu.id','tpk_out_stock.jenis_kayu');
    }
    public function stock_item(){
        return $this->hasMany('App\Http\Models\LogpondStock', 'from_order', 'id')->join('kategori_kayu','kategori_kayu.id','logpond_stock.jenis_kayu');

    }
    public function created_user(){
        return $this->hasOne('App\User','id',"created_by");
    }

    public function accepted_user(){
        return $this->hasOne('App\User','id',"accepted_by");
    }


}
