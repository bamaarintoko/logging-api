<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_jenis_kayu
 * @property string $kode_kategori
 * @property string $nama_kategori
 * @property string $created_at
 * @property int $created_by
 */
class KategoriKayu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kategori_kayu';

    /**
     * @var array
     */
    protected $fillable = ['id_jenis_kayu',
        'kode_kategori',
        'nama_kategori',
        'created_at',
        'created_by'];

    public $timestamps = false;
}
