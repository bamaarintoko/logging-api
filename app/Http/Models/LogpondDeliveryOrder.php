<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ld_id
 * @property string $reference_number
 * @property string $destination_name
 * @property string $driver_name
 * @property string $police_number
 * @property string $phone_number
 * @property string $status
 * @property int $created_by
 * @property string $created_date
 * @property string $shipped_date
 */
class LogpondDeliveryOrder extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logpond_delivery_order';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ld_id';

    /**
     * @var array
     */
    protected $fillable = ['reference_number',
        'destination_name',
        'driver_name',
        'police_number',
        'phone_number',
        'status',
        'created_by',
        'created_date',
        'shipped_date'];

    public function out_item()
    {
        return $this->hasMany('App\Http\Models\LogpondOut', 'reference_number', 'ld_id')->join('kategori_kayu', 'kategori_kayu.id', 'logpond_out.jenis_kayu');
    }

    public function created_user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function accepted_user()
    {
        return $this->hasOne('App\User', 'id', "accepted_by");
    }
}
