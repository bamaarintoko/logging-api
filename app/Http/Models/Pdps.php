<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $invoice_number_at
 * @property string $invoice_number_mt
 * @property string $note
 * @property string $status
 * @property int $created_by
 * @property string $created_date
 * @property string $updated_date
 */
class Pdps extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['invoice_number_at', 'invoice_number_mt', 'note', 'status', 'created_by', 'created_date', 'updated_date'];

    public function item(){
        return $this->hasMany('App\Http\Models\IncomingInventory', 'pdps_id', 'id')->join('master_inventory','master_inventory.id','incoming_inventory.item_id');
    }
}
