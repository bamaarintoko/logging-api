<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $setting_id
 * @property string $label
 * @property string $value
 */
class Settings extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'setting_id';

    /**
     * @var array
     */
    protected $fillable = ['label', 'value'];

}
