<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $panjang
 * @property int $satuan
 */
class PanjangKayu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'panjang_kayu';

    /**
     * @var array
     */
    protected $fillable = ['panjang', 'satuan'];

}
