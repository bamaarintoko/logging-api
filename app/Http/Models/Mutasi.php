<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $mutasi_id
 * @property int $amount
 * @property string $note
 * @property string $type
 * @property int $created_by
 * @property string $created_date
 */
class Mutasi extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mutasi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'mutasi_id';

    /**
     * @var array
     */
    protected $fillable = ['amount', 'note', 'type', 'created_by', 'created_date'];

}
