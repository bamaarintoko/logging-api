<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 2/18/2019
 * Time: 8:07 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $input             = $request->all();
        $input['password'] = bcrypt($input['password']);
        return $input;
    }

    public function login(Request $request)
    {
        $input = $request->all();
        if (Auth::attempt(['email' => request('email'),
            'password' => request('password')])) {
            $ip                 = $_SERVER["REMOTE_ADDR"];
            $user               = Auth::user();
            $user->ip           = $ip;
//            $user->phone_model  = $input["phone_model"];
            $user->phone_model  = '-';
//            $user->phone_number = $input["phone_number"];
            $user->phone_number = '-';
//            $user->player_id    = $input["player_id"];
            $user->player_id    = '-';
//        return $user->createToken('nApp')->accessToken;

            if ($user->status === "active") {
                $user->update();
                $success['token'] = $user->createToken('nApp')->accessToken;
                $success['user']  = $user;
                $data["status"]   = true;
                $data["message"]  = "login success";
                $data["result"]   = $success;
                $data["kode"]     = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "You are currently blocked by administrator.";
                $data["result"]  = [];
                $data["kode"]    = 200;
            }
        } else {
            $data["status"]  = false;
            $data["message"] = "Unauthorized.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        }

        return $data;
    }

    public function test()
    {
        return "qwerty";
    }
}