<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 2/19/2019
 * Time: 4:26 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Models\JenisKayu;
use App\Http\Models\KategoriKayu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    public function add_kategori(Request $request)
    {
        $validator = Validator::make($request->all(), ['par_jenis_id' => 'required',
            'par_kode' => 'required',
            'par_nama' => 'required',]);
        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "missing parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }
        //        return date("Y-m-d H:i:s");
        $kategori_kayu = new KategoriKayu();

        $jenis_id = $request->input('par_jenis_id');
        $kode     = $request->input('par_kode');
        $kategori = $request->input("par_nama");
        if (KategoriKayu::where('kode_kategori', $kode)->count() > 0) {
            $data = array("status" => false,
                "message" => "kode sudah ada",
                "kode" => 401,
                "result" => []);
            return $data;
        }

        $kategori_kayu->id_jenis_kayu = $jenis_id;
        $kategori_kayu->kode_kategori = $kode;
        $kategori_kayu->nama_kategori = $kategori;
        $kategori_kayu->created_by    = 0;
        $kategori_kayu->created_at    = date("Y-m-d H:i:s");
        if ($kategori_kayu->save()) {
            $data = array("status" => true,
                "message" => "tambah kategori berhasil",
                "kode" => 200,
                "result" => []);
        } else {
            $data = array("status" => false,
                "message" => "tambah kategori gagal",
                "kode" => 401,
                "result" => []);
        }
        return $data;
    }

    public function get_kategori(Request $request)
    {
        $validator = Validator::make($request->all(), ['par_jenis_id' => 'required']);
        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "missing parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }
        $jenis = $request->input('par_jenis_id');
        $kat   = JenisKayu::with(['kategori'])->where('id', $jenis)->first();
        $data  = array("status" => true,
            "message" => "kategori tersedia",
            "kode" => 200,
            "result" => $kat);
        //        echo $data;
        return $data;
    }
}