<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 2/18/2019
 * Time: 9:20 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Models\Buyer;
use App\Http\Models\JenisKayu;
use App\Http\Models\KategoriKayu;
use App\Http\Models\PanjangKayu;
use App\Http\Models\Tarif;
use App\Http\Models\TpkDeliveryOrder;
use App\Http\Models\TpkOutStock;
use App\Http\Models\TpkPaidStock;
use App\Http\Models\TpkRawStock;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TpkController extends Controller
{
    private $date, $user;

    public function __construct()
    {
        $this->date = date("Y-m-d H:i:s");
        $this->user = Auth::user();
    }

    public function notif()
    {

        send_notif();
    }

    public function insert_raw_stock(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_jenis' => 'required',
            'par_panjang' => 'required',
            'par_volume' => 'required',
            'par_jumlah' => 'required',
            'par_kondisi' => 'required',]);

        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "missing parameter",
                "result" => []);
            return $data;
        }

        $tpk_raw_stock                = new TpkRawStock();
        $tpk_raw_stock->jenis_kayu    = $input['par_jenis'];
        $tpk_raw_stock->panjang_kayu  = $input['par_panjang'];
        $tpk_raw_stock->volume        = $input['par_volume'];
        $tpk_raw_stock->jumlah_batang = $input['par_jumlah'];
        $tpk_raw_stock->kondisi       = $input['par_kondisi'];
        $tpk_raw_stock->created_at    = date("Y-m-d H:i:s");
        $tpk_raw_stock->created_by    = Auth::user()->id;

        if ($tpk_raw_stock->save()) {
            $data = array("status" => true,
                "message" => "tambah stock berhasil",
                "kode" => 200,
                "result" => []);
        } else {
            $data = array("status" => false,
                "message" => "tambah stock gagal",
                "kode" => 401,
                "result" => []);

        }
        //        $user                         = Auth::user()->id;
        return $data;
    }

    public function get_raw_stock(Request $request)
    {
        $input = $request->all();
        if (!$input) {
            $input['type'] = "all";
        }
        if ($input['type'] === 'kategori') {
            $raw_stock = TpkRawStock::groupBy("jenis_kayu")->selectRaw('sum(tpk_raw_stock.jumlah_batang) - IFNULL(bin.jb,0) as jumlah_batang, sum(tpk_raw_stock.volume) - IFNULL(bin.vb,0) as volume, bin.vb,
                tpk_raw_stock.jenis_kayu, nama_kategori, IFNULL(bin.jb,0) as jml')->join('kategori_kayu', 'kategori_kayu.id', 'tpk_raw_stock.jenis_kayu')->leftJoin(DB::raw("(SELECT *, IFNULL(sum(jumlah_batang),0)as jb,IFNULL(sum(volume),0)as vb FROM tpk_paid_stock GROUP BY jenis_kayu) bin"), function ($join) {
                $join->on('bin.jenis_kayu', '=', 'tpk_raw_stock.jenis_kayu');
            })->get();
        } elseif ($input['type'] === 'panjang') {
            $raw_stock_ = TpkRawStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "jenis_kayu")->get();
            $raw_stockk = TpkRawStock::groupBy("tpk_raw_stock.jenis_kayu", "tpk_raw_stock.panjang_kayu")->selectRaw("tpk_raw_stock.panjang_kayu,tpk_raw_stock.jenis_kayu,tpk_raw_stock.jumlah_batang, kategori_kayu.nama_kategori, sum(tpk_raw_stock.jumlah_batang) - IFNULL(bin.jb,0) as total,IFNULL(bin.jb,0) AS jb,IFNULL(bin.vb_paid,0) AS vb_paid , sum(tpk_raw_stock.volume) as volume_raw, sum(tpk_raw_stock.volume) - IFNULL(bin.vb_paid,0) as volume")->join('kategori_kayu', "kategori_kayu.id", "tpk_raw_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb, sum(volume) as vb_paid FROM tpk_paid_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                $join->on('bin.pk', '=', 'tpk_raw_stock.panjang_kayu');
                $join->on('bin.jk', '=', 'tpk_raw_stock.jenis_kayu');
            })->get();
            $raw_stock  = array();
            for ($x = 0; $x < count($raw_stock_); $x++) {
                $raw_stock[$x]["jenis_kayu"] = $raw_stock_[$x]->nama_kategori;
                for ($i = 0; $i < count($raw_stockk); $i++) {
                    if ($raw_stock_[$x]->jenis_kayu === $raw_stockk[$i]->jenis_kayu) {
                        $raw_stock[$x]['keterangan'][] = array("panjang" => $raw_stockk[$i]->panjang_kayu,
                            "raw_stock" => $raw_stockk[$i]->total,
                            "jumlah_batang" => $raw_stockk[$i]->jumlah_batang,
                            "volume" => $raw_stockk[$i]->volume,
                            "jumlah_batang_out" => $raw_stockk[$i]->jb,
                            "jenis_kayu" => $raw_stockk[$i]->jenis_kayu,);
                    }
                }
            }
            //            return $raw_stockk;
        } else {
            $raw_stock = TpkRawStock::with(['kategori'])->get();
        }

        if (count($raw_stock) > 0) {
            $data = array("status" => true,
                "message" => "stock tersedia",
                "result" => $raw_stock,
                "kode" => 200);
        } else {
            $data = array("status" => false,
                "message" => "stock tidak tersedia",
                "result" => [],
                "kode" => 401);
        }
        return $data;
    }

    public function get_total_raw_stock(Request $request)
    {
        $total = TpkRawStock::selectRaw('sum(jumlah_batang) as jumlah_batang')->first();
        //        return $total->jumlah_batang;
        if ($total) {
            $data = array("status" => true,
                "message" => "stock tersedia",
                "result" => $total,
                "kode" => 200);
        } else {
            $data = array("status" => false,
                "message" => "stock tidak tersedia",
                "result" => $total,
                "kode" => 401);
        }

        return $data;
    }

    public function insert_paid_stock(Request $request)
    {
        $input = $request->all();
        //        $data = array();
        $validator = Validator::make($request->all(), ['par_invoice' => 'required',
            'par_jenis' => 'required',
            'par_panjang' => 'required',
            'par_volume' => 'required',
            'par_jumlah' => 'required',]);

        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "missing parameter",
                "result" => [],
                "kode" => 401);
            return $data;
        }
        $cek_raw_stock = TpkRawStock::selectRaw("sum(jumlah_batang) as jumlah_batang")->where('jenis_kayu', $input['par_jenis'])->first();
        //        return $cek_raw_stock;
        if ($cek_raw_stock->jumlah_batang < $input['par_jumlah']) {
            $data = array("status" => false,
                "message" => "stock kayu tidak cukup",
                "result" => [],
                "kode" => 401);
            return $data;
        } else {
            $cek_raw_stock_meter = TpkRawStock::groupBy("panjang_kayu", "jenis_kayu")->selectRaw('sum(jumlah_batang) - IFNULL(bin.jb,0) as jumlah_batang,panjang_kayu,jenis_kayu, bin.*')->where("panjang_kayu", $input['par_panjang'])->where("jenis_kayu", $input['par_jenis'])->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb FROM tpk_paid_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                $join->on('bin.pk', "=", "tpk_raw_stock.panjang_kayu");
                $join->on("bin.jk", "=", "tpk_raw_stock.jenis_kayu");
            })->first();
            //            return $cek_raw_stock_meter;
            if ($cek_raw_stock_meter->jumlah_batang < $input['par_jumlah']) {
                $data = array("status" => false,
                    "message" => "stock kayu tidak cukup",
                    "result" => [],
                    "kode" => 401);
                return $data;
            }
            //            return $data;
            //            return $cek_raw_stock_meter;
        }

        //        return $data;
        //        return "halooooo";
        $tpk_paid_stock                = new TpkPaidStock();
        $tpk_paid_stock->invoice_id    = $input['par_invoice'];
        $tpk_paid_stock->jenis_kayu    = $input['par_jenis'];
        $tpk_paid_stock->panjang_kayu  = $input['par_panjang'];
        $tpk_paid_stock->jumlah_batang = $input['par_jumlah'];
        $tpk_paid_stock->volume        = $input['par_volume'];
        $tpk_paid_stock->created_at    = date("Y-m-d H:i:s");
        $tpk_paid_stock->created_by    = Auth::user()->id;

        if ($tpk_paid_stock->save()) {
            $data = array("status" => true,
                "message" => "tambah stock berhasil",
                "kode" => 200,
                "result" => []);
        } else {
            $data = array("status" => false,
                "message" => "tambah stock gagal",
                "kode" => 401,
                "result" => []);

        }
        return $data;
    }

    public function get_paid_stock(Request $request)
    {
        $input = $request->all();
        if (!$input) {
            $input['type'] = "all";
        }
        if ($input['type'] === 'kategori') {
            $paid_stock = TpkPaidStock::groupBy("jenis_kayu")->selectRaw('sum(jumlah_batang) as jumlah_batang,jenis_kayu,nama_kategori')->join('kategori_kayu', 'kategori_kayu.id', 'jenis_kayu')->get();
        } else if ($input['type'] === 'panjang') {
            $paid_stock_ = TpkPaidStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "jenis_kayu")->get();
            $paid_stockk = TpkPaidStock::groupBy("tpk_paid_stock.jenis_kayu", "tpk_paid_stock.panjang_kayu")->selectRaw("tpk_paid_stock.panjang_kayu,tpk_paid_stock.jenis_kayu, sum(tpk_paid_stock.jumlah_batang)as jumlah_batang,sum(tpk_paid_stock.volume) - IFNULL(bin.vb,0)as volume, kategori_kayu.nama_kategori, IFNULL(bin.jb,0) as jb,sum(tpk_paid_stock.jumlah_batang) - IFNULL(bin.jb,0) as jumlah_paid_stock")->join("kategori_kayu", "kategori_kayu.id", "tpk_paid_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb, sum(volume) as vb FROM tpk_out_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                $join->on('bin.pk', "=", "tpk_paid_stock.panjang_kayu");
                $join->on("bin.jk", "=", "tpk_paid_stock.jenis_kayu");
            })->get();
            $paid_stock  = array();
            //            return $paid_stockk;
            for ($x = 0; $x < count($paid_stock_); $x++) {
                $paid_stock[$x]["jenis_kayu"] = $paid_stock_[$x]->nama_kategori;
                for ($i = 0; $i < count($paid_stockk); $i++) {
                    if ($paid_stock_[$x]->jenis_kayu === $paid_stockk[$i]->jenis_kayu) {
                        $paid_stock[$x]['keterangan'][] = array("panjang" => $paid_stockk[$i]->panjang_kayu,
                            "paid_stock" => $paid_stockk[$i]->jumlah_paid_stock,
                            "jumlah_batang" => $paid_stockk[$i]->jumlah_batang,
                            "jumlah_batang_out" => $paid_stockk[$i]->jb,
                            "volume" => $paid_stockk[$i]->volume,
                            "jenis_kayu" => $paid_stockk[$i]->jenis_kayu,);
                    }
                }
            }

            //            return $paid_stock;
        } else {

            $paid_stock = TpkPaidStock::with(['kategori'])->get();
        }

        if (count($paid_stock) > 0) {
            $data = array("status" => true,
                "message" => "stock tersedia",
                "result" => $paid_stock,
                "kode" => 200);
        } else {
            $data = array("status" => false,
                "message" => "stock tidak tersedia",
                "result" => [],
                "kode" => 401);
        }
        return $data;
    }

    public function insert_out_stock(Request $request)
    {
        $input = $request->all();
        //        $validator      = Validator::make($request->all(), ['reference_number' => 'required',
        //            'par_jenis' => 'required',
        //            'par_panjang' => 'required',
        //            'par_volume' => 'required',
        //            'par_jumlah' => 'required',]);
        //        $tpk_paid_stock = TpkPaidStock::selectRaw("sum(jumlah_batang) as jumlah_batang")->groupBy("jenis_kayu", "panjang_kayu")->where('jenis_kayu', $input['par_jenis'])->where('panjang_kayu', $input['par_panjang'])->first();
        //        if ($input['par_jumlah'] > $tpk_paid_stock->jumlah_batang) {
        //            $data['status']  = false;
        //            $data['message'] = "stock tidak mencukupi";
        //            $data['result']  = $tpk_paid_stock;
        //            $data['kode']    = 401;
        //
        //            return $data;
        //        }
        //        $tpk_out_stock                   = new TpkOutStock();
        //        $tpk_out_stock->reference_number = $input['reference_number'];
        //        $tpk_out_stock->jenis_kayu       = $input['par_jenis'];
        //        $tpk_out_stock->panjang_kayu     = $input['par_panjang'];
        //        $tpk_out_stock->volume           = $input['par_volume'];
        //        $tpk_out_stock->jumlah_batang    = $input['par_jumlah'];
        //        $tpk_out_stock->created_at       = date("Y-m-d H:i:s");
        //        $tpk_out_stock->created_by       = Auth::user()->id;
        //        if ($tpk_out_stock->save()) {
        //            $data['status']  = true;
        //            $data['message'] = "tambah stock berhasil";
        //            $data['result']  = [];
        //            $data['kode']    = 200;
        //        } else {
        //            $data['status']  = false;
        //            $data['message'] = "tambah stock gagal";
        //            $data['result']  = [];
        //            $data['kode']    = 401;
        //        }

        return json_decode($input['par_arr']);

    }

    public function create_surat_jalan(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_nomor' => 'required',
            'par_tujuan' => 'required',
            'par_nama_tujuan' => 'required',
            'par_buyer' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $cek_surat = TpkDeliveryOrder::where("out_status", "process")->first();
            if ($cek_surat) {
                $data["status"]  = false;
                $data["message"] = "Terdapat surat jalan yang masih dalam proses. Silahkan kirim dahulu";
                $data["result"]  = $cek_surat;
                $data["kode"]    = 401;
            } else {
                $cek_no_surat = TpkDeliveryOrder::where("reference_number", $input["par_nomor"])->first();

                if ($cek_no_surat) {
                    $data["status"]  = false;
                    $data["message"] = "duplikat nomor surat.";
                    $data["result"]  = [];
                    $data["kode"]    = 401;
                } else {
                    $delivery                   = new TpkDeliveryOrder();
                    $delivery->reference_number = $input['par_nomor'];
                    $delivery->out_status       = "process";
                    $delivery->destination      = $input['par_tujuan'];
                    $delivery->dest_name        = $input["par_nama_tujuan"];
                    $delivery->created_at       = date("Y-m-d H:i:s");
                    $delivery->process_date     = date("Y-m-d H:i:s");
                    $delivery->created_by       = Auth::user()->id;
                    $delivery->buyer_id         = $input['par_buyer'];
                    if ($delivery->save()) {
                        $data["status"]  = true;
                        $data["message"] = "pembuatan surat jalan berhasil";
                        $data["result"]  = $delivery;
                        $data["kode"]    = 200;
                    } else {
                        $data["status"]  = false;
                        $data["message"] = "pembuatan surat jalan gagal";
                        $data["result"]  = [];
                        $data["kode"]    = 401;
                    }
                }
            }
        }

        return $data;
    }

    public function destroy_surat_jalan(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id_surat' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            TpkDeliveryOrder::where("id", $input["par_id_surat"])->delete();
            TpkOutStock::where("reference_number", $input["par_id_surat"])->delete();
            $data["status"]  = true;
            $data["message"] = "surat jalan berhasil dihapus.";
            $data["result"]  = [];
            $data["kode"]    = 200;
        }

        return $data;
    }

    public function destroy_item(Request $request)
    {
        $input = $request->all();

        if (TpkOutStock::where("id", $input['par_id'])->delete()) {
            $data["status"]  = true;
            $data["message"] = "data berhasil dihapus";
            $data["result"]  = [];
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "data gagal dihapus";
            $data["result"]  = [];
            $data["kode"]    = 401;
        }

        return $data;
    }

    public function cek_surat_jalan(Request $request)
    {
        $cek = TpkDeliveryOrder::with(["out_item" => function ($query) {
            $query->select("tpk_out_stock.reference_number", "tpk_out_stock.panjang_kayu as par_panjang", "tpk_out_stock.jumlah_batang as par_jumlah", "tpk_out_stock.volume as par_volume", "kategori_kayu.nama_kategori as jenis_kayu_label", "tpk_out_stock.id");
        }])->select("tpk_delivery_order.*", "buyer.*")->join("buyer", "tpk_delivery_order.buyer_id", "buyer.buyer_id")->where("out_status", "process")->first();
        if ($cek) {
            $data["status"]  = true;
            $data["message"] = "terdapat surat jalan dengan status process";
            $data["result"]  = $cek;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "tidak ada surat jalan dengan status process";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }
        return $data;
    }

    public function detail_tpk_surat_jalan(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_surat_id' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $cek = TpkDeliveryOrder::with(["out_item" => function ($query) {
                $query->select("tpk_out_stock.reference_number", "tpk_out_stock.panjang_kayu as par_panjang", "tpk_out_stock.jumlah_batang as par_jumlah", "tpk_out_stock.volume as par_volume", "kategori_kayu.nama_kategori as jenis_kayu_label", "tpk_out_stock.id");
            }])->select("tpk_delivery_order.*", "buyer.*")->join("buyer", "tpk_delivery_order.buyer_id", "buyer.buyer_id")->where("tpk_delivery_order.id", $input['par_surat_id'])->first();
            //        die("qwe");
            if ($cek) {
                $data["status"]  = true;
                $data["message"] = "data tersedia.";
                $data["result"]  = $cek;
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "data tidak tersedia.";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
        }
        return $data;
    }

    public function update_surat_jalan(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_surat_id' => 'required',
            'par_pol_number' => 'required',
            'par_driver' => 'required',
            'par_phone_number' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $update                = TpkDeliveryOrder::where("id", $input['par_surat_id'])->first();
            $update->driver_name   = $input["par_driver"];
            $update->police_number = $input["par_pol_number"];
            $update->phone_number  = $input["par_phone_number"];
            $update->created_by    = Auth::user()->id;
            if ($update->destination === "industri") {
                $update->arrived_date = date("Y-m-d H:i:s");
                $update->out_status   = "arrived";
            } else {
                $update->out_status = "shipping";

            }
            $update->shipping_date = date("Y-m-d H:i:s");
            if (!$update->file) {
                //                die("qwe");
                $data["status"]  = false;
                $data["message"] = "File tidak boleh kosong.";
                $data["result"]  = [];
                $data["kode"]    = 401;
            } else {
                if ($update->update()) {
                    $tarif                   = new Tarif();
                    $tarif->reference_number = $update->id;
                    $tarif->nomor_surat      = $update->reference_number;
                    $tarif->origin           = "tpk";
                    $tarif->destination      = strtoupper($update->destination) . ", " . strtoupper($update->dest_name);
                    $tarif->status           = "belum_dibayar";
                    if ($update->destination === "industri") {
                        $tarif->status_pengiriman = "diterima";
                        $tarif->arrived_date      = date("Y-m-d H:i:s");
                        $tarif->shipped_date      = date("Y-m-d H:i:s");
                    } else {
                        $tarif->status_pengiriman = "dikirim";
                        $tarif->shipped_date      = date("Y-m-d H:i:s");

                    }
                    $tarif->ammount    = 0;
                    $tarif->created_at = $this->date;
                    $tarif->created_by = Auth::user()->id;
                    $tarif->save();
                    $data["status"]  = true;
                    $data["message"] = "pengiriman berhasil.";
                    $data["result"]  = $update;
                    $data["kode"]    = 200;
                } else {
                    $data["status"]  = false;
                    $data["message"] = "gagal kirim.";
                    $data["result"]  = [];
                    $data["kode"]    = 401;
                }
            }

        }

        return $data;

    }

    public function create_out_item(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id_surat' => 'required',
            'par_jenis' => 'required',
            'par_panjang' => 'required',
            'par_volume' => 'required',
            'par_jumlah' => 'required',]);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $cek = TpkPaidStock::selectRaw("sum(jumlah_batang) - IFNULL(bin.jumlah_keluar,0) as stok_tersedia")->where("jenis_kayu", $input["par_jenis"])->where("panjang_kayu", $input["par_panjang"])->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jumlah_keluar FROM tpk_out_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                $join->on('bin.pk', "=", "tpk_paid_stock.panjang_kayu");
                $join->on("bin.jk", "=", "tpk_paid_stock.jenis_kayu");
            })->first();
            if ($input["par_jumlah"] > $cek->stok_tersedia) {
                $data["status"]  = false;
                $data["message"] = "stok tidak mencukupi.";
                $data["result"]  = [];
                $data["kode"]    = 401;
            } else {
                //            return $cek;
                $tpk_out                   = new TpkOutStock();
                $tpk_out->reference_number = $input['par_id_surat'];
                $tpk_out->jenis_kayu       = $input['par_jenis'];
                $tpk_out->panjang_kayu     = $input['par_panjang'];
                $tpk_out->volume           = $input['par_volume'];
                $tpk_out->jumlah_batang    = $input['par_jumlah'];
                $tpk_out->created_by       = Auth::user()->id;
                $tpk_out->created_at       = date("Y-m-d H:i:s");
                if ($tpk_out->save()) {
                    $data["status"]  = true;
                    $data["message"] = "input data berhasil";
                    $data["result"]  = $tpk_out;
                    $data["kode"]    = 200;
                } else {
                    $data["status"]  = false;
                    $data["message"] = "input data gagal";
                    $data["result"]  = [];
                    $data["kode"]    = 401;
                }
            }
        }

        return $data;

    }

    public function get_jenis_kayu()
    {
        $panjang    = PanjangKayu::get();
        $jenis      = KategoriKayu::get();
        $jenisArr   = array();
        $panjangArr = array();
        for ($x = 0; $x < count($panjang); $x++) {
            $panjangArr[$x]['label'] = $panjang[$x]->panjang;
            $panjangArr[$x]['value'] = $panjang[$x]->panjang;
        }
        for ($x = 0; $x < count($jenis); $x++) {
            $jenisArr[$x]['label'] = $jenis[$x]->nama_kategori;
            $jenisArr[$x]['value'] = $jenis[$x]->id;
        }
        $data['status']            = true;
        $data['message']           = "success";
        $data["result"]['panjang'] = $panjangArr;
        $data["result"]['jenis']   = $jenisArr;
        $data['kode']              = 200;
        return $data;
    }

    public function get_jenis_kayu_by_raw_stock()
    {
        $jenis    = TpkRawStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "tpk_raw_stock.jenis_kayu")->get();
        $jenisArr = array();
        if (count($jenis) > 0) {
            for ($x = 0; $x < count($jenis); $x++) {
                //                $panjang = TpkRawStock::select("panjang_kayu as value", "panjang_kayu as label")->where("jenis_kayu", $jenis[$x]->jenis_kayu)->groupBy("panjang_kayu")->get();
                //
                $jenisArr[$x]['label'] = $jenis[$x]->nama_kategori;
                $jenisArr[$x]['value'] = $jenis[$x]->id;
                //                $jenisArr[$x]['panjang'] = $panjang;
                $panjang = TpkRawStock::groupBy("tpk_raw_stock.jenis_kayu", "tpk_raw_stock.panjang_kayu")->selectRaw("tpk_raw_stock.panjang_kayu as label,tpk_raw_stock.panjang_kayu as value, sum(tpk_raw_stock.jumlah_batang) - IFNULL(bin.jb,0) as stock_tersedia,sum(tpk_raw_stock.volume) - IFNULL(bin.vb,0) as volume")->join('kategori_kayu', "kategori_kayu.id", "tpk_raw_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb,sum(volume) as vb FROM tpk_paid_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                    $join->on('bin.pk', '=', 'tpk_raw_stock.panjang_kayu');
                    $join->on('bin.jk', '=', 'tpk_raw_stock.jenis_kayu');
                })->where("jenis_kayu", $jenis[$x]->jenis_kayu)->get();
                //                $panjang = TpkRawStock::where("jenis_kayu", $jenis[$x]->jenis_kayu)->groupBy("panjang_kayu")->get();
                //                return $panjang;
                //                $jenisArr[$x]['label']   = $jenis[$x]->nama_kategori;
                //                $jenisArr[$x]['value']   = $jenis[$x]->id;
                //                $jenisArr[$x]['stock_tersedia']   = $jenis[$x]->id;
                $jenisArr[$x]['panjang'] = $panjang;
            }
            //                    return $jenisArr;

            $data['status']  = true;
            $data['message'] = "success";
            $data['result']  = $jenisArr;
            $data['kode']    = 200;
        } else {
            $data['status']  = false;
            $data['message'] = "Stock kayu tidak tersedia.";
            $data['result']  = [];
            $data['kode']    = 404;
        }
        return $data;
    }

    public function get_jenis_kayu_by_paid_stock()
    {

        $jenis    = TpkPaidStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "tpk_paid_stock.jenis_kayu")->get();
        $buyer    = Buyer::get();
        $jenisArr = array();
        if (count($jenis) > 0) {
            for ($x = 0; $x < count($jenis); $x++) {
                $jenisArr[$x]['label']   = $jenis[$x]->nama_kategori;
                $jenisArr[$x]['value']   = $jenis[$x]->id;
                $paid_stockk             = TpkPaidStock::groupBy("tpk_paid_stock.jenis_kayu", "tpk_paid_stock.panjang_kayu")->selectRaw("tpk_paid_stock.panjang_kayu as value,tpk_paid_stock.panjang_kayu as label, sum(tpk_paid_stock.jumlah_batang) - IFNULL(bin.jb,0) as jumlah_paid_stock, sum(tpk_paid_stock.volume) - IFNULL(bin.vb,0) as volume")->join("kategori_kayu", "kategori_kayu.id", "tpk_paid_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb, sum(volume) as vb FROM tpk_out_stock GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                    $join->on('bin.pk', "=", "tpk_paid_stock.panjang_kayu");
                    $join->on("bin.jk", "=", "tpk_paid_stock.jenis_kayu");
                })->where("jenis_kayu", $jenis[$x]->jenis_kayu)->get();
                $jenisArr[$x]['panjang'] = $paid_stockk;
            }
            $data['status']          = true;
            $data['message']         = "success";
            $data['result']['jenis'] = $jenisArr;
            $data['result']['buyer'] = $buyer;
            $data['kode']            = 200;
        } else {
            $data['status']  = false;
            $data['message'] = "Stock kayu tidak tersedia.";
            $data['result']  = [];
            $data['kode']    = 404;
        }


        //        $paid_stock  = array();
        return $data;
    }

//    function untuk mengambil jumlah stok yang sudah dan belum dibayar
    public function get_stock_kayu()
    {
        $paid_stock = TpkPaidStock::sum("jumlah_batang");
        $raw_stock  = TpkRawStock::sum("jumlah_batang");
        $out_stock  = TpkOutStock::sum("jumlah_batang");

        $data["status"]               = true;
        $data["message"]              = "success";
        $data["result"]["raw_stock"]  = $raw_stock - $paid_stock;
        $data["result"]["paid_stock"] = $paid_stock - $out_stock;
        $data["kode"]                 = 200;
        return $data;
    }

    public function get_shipping()
    {
        $process_deliver = TpkDeliveryOrder::get();
        if (count($process_deliver) > 0) {
            $data["status"]  = true;
            $data["message"] = "success";
            $data["result"]  = $process_deliver;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "Tidak ada data pengiriman.";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }
        return $data;
    }

    public function upload(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_nomor_surat' => 'required',
            'par_create_by' => 'required',
            'par_origin' => 'required',
            'par_id_surat' => 'required']);

        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $surat = TpkDeliveryOrder::where("id", $input['par_id_surat'])->first();
            //            return $surat;
            $ext  = ["docx",
                "xlsx",
                "xls",
                "doc"];
            $file = $request->file("file");
            //            return $file;
            $moveTo = public_path() . '/file';
            $size   = pow(1024, 3); // 512 KB
            $name   = time() . "-" . $input['par_origin'] . "-" . $surat->reference_number . "-" . $input['par_create_by'] . "." . $file->getClientOriginalExtension();
            if (in_array($file->getClientOriginalExtension(), $ext)) {
                if ($file->getSize() > $size) {
                    $data["status"]  = false;
                    $data["message"] = "File to big. Max size 2 mb";
                    $data["result"]  = [];
                    $data["kode"]    = 404;
                } else {
                    if ($file->move($moveTo, $name)) {
                        $surat->file = $name;
                        $surat->update();
                        $data["status"]  = true;
                        $data["message"] = "File uploaded";
                        $data["result"]  = [];
                        $data["kode"]    = 200;
                    } else {
                        $data["status"]  = false;
                        $data["message"] = "Error. File not uploaded";
                        $data["result"]  = [];
                        $data["kode"]    = 404;
                    }
                    //                    $file->move($moveTo, $name);
                }
                //            return $file->getSize();
            } else {
                $data["status"]  = false;
                $data["message"] = "File type not allowed. Must docx, xlsx, xlx, doc";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
        }
        return $data;
        //        return $file->getClientOriginalExtension();
    }

    public function delete(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_id_surat' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $tpk = TpkDeliveryOrder::where("id", $input['par_id_surat'])->first();
            if ($tpk->file) {
                $moveTo = public_path() . '/file/' . $tpk->file;
                if (unlink($moveTo)) {
                    $tpk->file = "";
                    $tpk->update();
                    $data["status"]  = true;
                    $data["message"] = "File deleted.";
                    $data["result"]  = [];
                    $data["kode"]    = 200;
                } else {
                    $data["status"]  = false;
                    $data["message"] = "File failed to delete.";
                    $data["result"]  = [];
                    $data["kode"]    = 401;
                }
                //                unlink($moveTo);

            } else {
                $data["status"]  = false;
                $data["message"] = "no file.";
                $data["result"]  = [];
                $data["kode"]    = 401;
            }
        }
        return $data;
    }

    public function download()
    {
        $moveTo  = public_path() . '/file/1552669252-tpk-0011-Moore.docx';
        $headers = array('Content-Type: application/pdf');

        return response()->download($moveTo, 'filename.docx', $headers);
    }

}