<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 3/16/2019
 * Time: 11:44 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Models\Settings;

class SettingController
{
    public function cek()
    {
//        die("-");
        $cek = Settings::get();

        $data["status"]  = true;
        $data["message"] = "ready";
        $data["result"]  = $cek;
        $data["kode"]    = 200;

        return $data;
    }
}