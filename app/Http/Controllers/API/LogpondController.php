<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 2/27/2019
 * Time: 10:47 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Models\LogpondDeliveryOrder;
use App\Http\Models\LogpondOut;
use App\Http\Models\LogpondStock;
use App\Http\Models\Tarif;
use App\Http\Models\TpkDeliveryOrder;
use App\Http\Models\TpkOutStock;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LogpondController extends Controller
{
    private $date, $user;

    public function __construct()
    {
        $this->date = date("Y-m-d H:i:s");
        $this->user = Auth::user();
    }

    public function get_logpond_shipping()
    {
        $arr = TpkDeliveryOrder::where("out_status", "shipping")->where("destination", "logpond")->get();

        if (count($arr) > 0) {
            $data["status"]  = true;
            $data["message"] = "ada proses pengiriman";
            $data["result"]  = $arr;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "tidak ada data pengiriman";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }

        return $data;
    }

    // api untuk menampilkan riwayat pengiriman dari logpond
    public function get_logpond_out_shipping(Request $request)
    {
        $delivery = LogpondDeliveryOrder::orderBy('created_date', "DESC")->get();
        if (count($delivery) > 0) {
            $data['status']  = true;
            $data["message"] = "ada pengiriman";
            $data["result"]  = $delivery;
            $data["kode"]    = 200;
        } else {
            $data['status']  = false;
            $data["message"] = "Tidak ada data pengiriman.";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }
        return $data;
    }

    public function get_detail(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_out_id' => 'required']);

        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            $out = TpkDeliveryOrder::with(['out_item'])->where("id", $input['par_out_id'])->first();

            if ($out) {
                $data["status"]  = true;
                $data["message"] = "data tersedia";
                $data["result"]  = $out;
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "tidak ada data";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
        }
        return $data;
    }

    //    api ambil detail logpond shiping
    public function get_detail_shipping(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_out_id' => 'required']);
        //        cek validasi
        if ($validator->fails()) {
            return message(false, $validator->messages()->all(), [], 404);
        }
        //        ambil data berdasarkan logpond_id
        $shipping = LogpondDeliveryOrder::with(['out_item',
            'created_user',
            'accepted_user'])->where('ld_id', $input['par_out_id'])->first();

        //        cek data shipping
        if ($shipping) {
            $data = message(true, "data tersedia", $shipping, 200);
        } else {
            $data = message(false, "data tidak tersedia", [], 404);

        }
        return $data;

    }

    public function get_logpond(Request $request)
    {
        $item          = LogpondStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "logpond_stock.jenis_kayu")->get();
        $item_out      = LogpondStock::groupBy("logpond_stock.jenis_kayu", "logpond_stock.panjang_kayu")->selectRaw("logpond_stock.panjang_kayu,logpond_stock.jenis_kayu, sum(logpond_stock.jumlah_batang)as jumlah_batang, kategori_kayu.nama_kategori, IFNULL(bin.jumlah_keluar,0) as jumlah_keluar,sum(logpond_stock.jumlah_batang) - IFNULL(bin.jumlah_keluar,0) as logpond_stock,sum(volume) - IFNULL(bin.volume_keluar,0) as volume")->join("kategori_kayu", "kategori_kayu.id", "logpond_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jumlah_keluar, sum(volume) as volume_keluar FROM logpond_out GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
            $join->on('bin.pk', "=", "logpond_stock.panjang_kayu");
            $join->on("bin.jk", "=", "logpond_stock.jenis_kayu");
        })->get();
        $logpond_stock = array();
        for ($x = 0; $x < count($item); $x++) {
            $logpond_stock[$x]["jenis_kayu"] = $item[$x]->nama_kategori;
            for ($i = 0; $i < count($item_out); $i++) {
                if ($item[$x]->jenis_kayu === $item_out[$i]->jenis_kayu) {
                    $logpond_stock[$x]['keterangan'][] = array("panjang" => $item_out[$i]->panjang_kayu,
                        "logpond_stock" => $item_out[$i]->logpond_stock,
                        "jumlah_keluar" => $item_out[$i]->jumlah_keluar,
                        "volume" => $item_out[$i]->volume,
                        "jenis_kayu" => $item_out[$i]->jenis_kayu,);
                }
            }
        }

        if (count($item) > 0) {
            $data["status"]  = true;
            $data["message"] = "stock tersedia";
            $data["result"]  = $logpond_stock;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "stock tidak tersedia";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }

        return $data;
    }

    public function update_shipping(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_out_id' => 'required',
            "par_arr" => "required"]);

        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            $update               = TpkDeliveryOrder::where("id", $input["par_out_id"])->first();
            $update->out_status   = "arrived";
            $update->arrived_date = date("Y-m-d H:i:s");
            $update->accepted_by  = Auth::user()->id;
            $user                 = User::select("player_id")->whereIn("role", ["kasir",
                "tpk_review"])->get();
            $idArr                = array();
            for ($i = 0; $i < count($user); $i++) {
                if ($user[$i]->player_id) {
                    $idArr[] = $user[$i]->player_id;
                }
            }
            //            return $idArr;
            $item = json_decode($input["par_arr"]);
            for ($x = 0; $x < count($item); $x++) {
                $save_to_logpond                = new LogpondStock();
                $save_to_logpond->jenis_kayu    = $item[$x]->jenis_kayu;
                $save_to_logpond->panjang_kayu  = $item[$x]->panjang_kayu;
                $save_to_logpond->jumlah_batang = $item[$x]->jumlah_batang;
                $save_to_logpond->volume        = $item[$x]->volume;
                $save_to_logpond->catatan       = $item[$x]->catatan;
                $save_to_logpond->created_at    = date("Y-m-d H:i:s");
                $save_to_logpond->created_by    = Auth::user()->id;
                $save_to_logpond->from_order    = $input["par_out_id"];
                $save_to_logpond->save();
                //                echo $x;
            }
            //            return $item;
            if ($update->update()) {
                send_notif("Pengiriman " . $update->reference_number, "Pengiriman dengan nomor surat " . $update->reference_number . " telah diterima.", $idArr);

                $tarif                    = Tarif::where("origin", "tpk")->where("reference_number", $update->id)->first();
                $tarif->status_pengiriman = "diterima";
                $tarif->arrived_date      = date("Y-m-d H:i:s");
                $tarif->created_by        = Auth::user()->id;
                $tarif->update();
                $data["status"]  = true;
                $data["message"] = "konfirmasi pengiriman dengan nomor surat : " . $update->reference_number . " berhasil";
                $data["result"]  = [];
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "konfirmasi gagal";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
        }
        return $data;
    }

    public function log_letter(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_nomor' => 'required',
            'par_nama_tujuan' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            $delivery                   = new LogpondDeliveryOrder();
            $delivery->reference_number = $input["par_nomor"];
            $delivery->destination_name = $input["par_nama_tujuan"];
            $delivery->status           = "process";
            $delivery->created_date     = date("Y-m-d H:i:s");
            $delivery->created_by       = Auth::user()->id;

            if ($delivery->save()) {
                $data["status"]  = true;
                $data["message"] = "surat jalan berhasil dibuat";
                $data["result"]  = $delivery;
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "pembuatan surat jalan gagal.";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }

        }
        return $data;
    }

    public function destroy_log_letter(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id_surat' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            LogpondDeliveryOrder::where("ld_id", $input["par_id_surat"])->delete();
            LogpondOut::where("reference_number", $input["par_id_surat"])->delete();
            //            TpkOutStock::where("reference_number", $input["par_id_surat"])->delete();
            $data["status"]  = true;
            $data["message"] = "surat jalan berhasil dihapus.";
            $data["result"]  = [];
            $data["kode"]    = 200;
        }

        return $data;
    }

    public function cek_log_letter(Request $request)
    {
        $cek = LogpondDeliveryOrder::with(["out_item" => function ($query) {
            $query->select("logpond_out.reference_number", "logpond_out.panjang_kayu as par_panjang", "logpond_out.jumlah_batang as par_jumlah", "logpond_out.volume as par_volume", "kategori_kayu.nama_kategori as jenis_kayu_label", "logpond_out.logpond_out_id");
        }])->where("status", "process")->first();
        if ($cek) {
            $data["status"]  = true;
            $data["message"] = "terdapat surat jalan dengan status process";
            $data["result"]  = $cek;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "tidak ada surat jalan dengan status process";
            $data["result"]  = [];
            $data["kode"]    = 404;
        }
        return $data;
    }

    //    API untuk menambahkan item kayu LHP ke table logpond_out
    public function out_log_item(Request $request)
    {
        //        return $this->user;
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id_surat' => 'required',
            'par_jenis' => 'required',
            'par_panjang' => 'required',
            'par_volume' => 'required',
            'par_jumlah' => 'required',]);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $cek = LogpondStock::selectRaw("sum(jumlah_batang) - IFNULL(bin.jumlah_keluar,0) as stok_logpond")->where("jenis_kayu", $input["par_jenis"])->where("panjang_kayu", $input["par_panjang"])->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jumlah_keluar FROM logpond_out GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                $join->on('bin.pk', "=", "logpond_stock.panjang_kayu");
                $join->on("bin.jk", "=", "logpond_stock.jenis_kayu");
            })->first();
            //            return $cek;
            if ($input["par_jumlah"] > $cek->stok_logpond) {
                $data["status"]  = false;
                $data["message"] = "stok tidak mencukupi.";
                $data["result"]  = [];
                $data["kode"]    = 401;
            } else {
                // cek apakah ada jenis kayu, reference_number dan panjang kayu yang sama
                $cek_item = LogpondOut::where('reference_number', $input['par_id_surat'])->where('jenis_kayu', $input['par_jenis'])->where('panjang_kayu', $input['par_panjang'])->first();

                // jika ada, maka tambahkan jumlah sebelumnya dengan yang baru
                if ($cek_item) {
                    $cek_item->jumlah_batang = $cek_item->jumlah_batang + $input['par_jumlah'];
                    $cek_item->volume        = $cek_item->volume + $input['par_volume'];
                    if ($cek_item->update()) {
                        return message(true, 'data berhasil diupdate', $cek_item, 200);
                    } else {
                        return message(false, 'data gagal diupdate', [], 404);
                    }
                    // jika tidak ada, maka input item baru
                } else {
                    $logpond_out                   = new LogpondOut();
                    $logpond_out->reference_number = $input["par_id_surat"];
                    $logpond_out->jenis_kayu       = $input["par_jenis"];
                    $logpond_out->panjang_kayu     = $input["par_panjang"];
                    $logpond_out->volume           = $input["par_volume"];
                    $logpond_out->jumlah_batang    = $input["par_jumlah"];
                    $logpond_out->created_at       = $this->date;
                    $logpond_out->created_by       = Auth::user()->id;
                    if ($logpond_out->save()) {
                        $data["status"]  = true;
                        $data["message"] = "data berhasil ditambah.";
                        $data["result"]  = $logpond_out;
                        $data["kode"]    = 200;
                    } else {
                        $data["status"]  = false;
                        $data["message"] = "data gagal ditambah.";
                        $data["result"]  = [];
                        $data["kode"]    = 401;
                    }
                }
            }
        }

        return $data;
    }

    //    API untuk menghapus data item di table logpond_out berdasarkan logpond_out_id
    public function delete_log_item(Request $request)
    {
        //        return "wr";
        $input     = $request->all();
        $validator = Validator::make($input, ['par_log_id' => 'required']);
        //        cek validasi
        if ($validator->fails()) {
            return message(false, $validator->messages()->all(), [], 404);
        }
        //        cek data
        $data = LogpondOut::find($input['par_log_id']);
        if ($data) {
            //            hapus data
            if ($data->delete()) {
                $data = message(true, 'data berhasil dihapus', [], 200);
            } else {
                $data = message(false, 'data gagal dihapus', [], 404);
            }
        } else {
            $data = message(false, 'tidak ada data', [], 404);
        }
        return $data;
    }

    public function log_pond_attr(Request $request)
    {
        $jenis    = LogpondStock::groupBy("jenis_kayu")->join("kategori_kayu", "kategori_kayu.id", "logpond_stock.jenis_kayu")->get();
        $jenisArr = array();
        if (count($jenis) > 0) {
            for ($x = 0; $x < count($jenis); $x++) {
                $jenisArr[$x]['label']   = $jenis[$x]->nama_kategori;
                $jenisArr[$x]['value']   = $jenis[$x]->id;
                $paid_stockk             = LogpondStock::groupBy("logpond_stock.jenis_kayu", "logpond_stock.panjang_kayu")->selectRaw("logpond_stock.panjang_kayu as value,logpond_stock.panjang_kayu as label, sum(logpond_stock.jumlah_batang) - IFNULL(bin.jb,0) as jumlah_paid_stock, sum(logpond_stock.volume) - IFNULL(bin.vb,0) as volume")->join("kategori_kayu", "kategori_kayu.id", "logpond_stock.jenis_kayu")->leftJoin(DB::raw("(SELECT panjang_kayu as pk, jenis_kayu as jk,sum(jumlah_batang) as jb, sum(volume) as vb FROM logpond_out GROUP BY jenis_kayu,panjang_kayu) bin"), function ($join) {
                    $join->on('bin.pk', "=", "logpond_stock.panjang_kayu");
                    $join->on("bin.jk", "=", "logpond_stock.jenis_kayu");
                })->where("jenis_kayu", $jenis[$x]->jenis_kayu)->get();
                $jenisArr[$x]['panjang'] = $paid_stockk;
            }
            $data['status']  = true;
            $data['message'] = "success";
            $data['result']  = $jenisArr;
            $data['kode']    = 200;
        } else {
            $data['status']  = false;
            $data['message'] = "Stock kayu tidak tersedia.";
            $data['result']  = [];
            $data['kode']    = 404;
        }


        //        $paid_stock  = array();
        return $data;
    }

    public function update_letter(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_surat_id' => 'required',
            'par_pol_number' => 'required',
            'par_driver' => 'required',
            'par_phone_number' => 'required']);

        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            //            $update = LogpondDeliveryOrder::where("ld_id",$input["par_id_surat"])->first();
            $update = new LogpondDeliveryOrder();

            $update->police_number = $input["par_pol_number"];
            $update->driver_name   = $input["par_driver"];
            $update->phone_number  = $input["par_phone_number"];
            $update->status        = "shipped";
            $update->shipped_date  = date("Y-m-d H:i:s");
            $update->created_by    = Auth::user()->id;
            if ($update->update()) {
                $data["status"]  = true;
                $data["message"] = "konfirmasi pengiriman berhasil";
                $data["result"]  = [];
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "konfirmasi gagal";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
            return $data;
        }

    }

    public function add_log_out(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id_surat' => 'required',
            'par_jenis' => 'required',
            'par_panjang' => 'required',
            'par_volume' => 'required',
            'par_jumlah' => 'required',]);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            $logpond                   = new LogpondOut();
            $logpond->reference_number = $input["par_id_surat"];
            $logpond->jenis_kayu       = $input["par_jenis"];
            $logpond->panjang_kayu     = $input["par_panjang"];
            $logpond->volume           = $input["par_volume"];
            $logpond->jumlah_kayu      = $input["jumlah_kayu"];
            $logpond->created_at       = date("Y-m-d H:i:s");
            $logpond->created_by       = Auth::user()->id;
            if ($logpond->save()) {
                $data["status"]  = true;
                $data["message"] = "item berhasil ditambahkan";
                $data["result"]  = $logpond;
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "item gagal ditambahkan";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }

        }

    }

    public function update_log_surat_jalan(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_surat_id' => 'required',
            'par_pol_number' => 'required',
            'par_driver' => 'required',
            'par_phone_number' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        } else {
            $update                = LogpondDeliveryOrder::where("ld_id", $input['par_surat_id'])->first();
            $update->driver_name   = $input["par_driver"];
            $update->police_number = $input["par_pol_number"];
            $update->phone_number  = $input["par_phone_number"];
            $update->status        = "shipped";
            $update->created_by    = Auth::user()->id;
            $update->shipped_date  = date("Y-m-d H:i:s");

            if ($update->update()) {
                $tarif                    = new Tarif();
                $tarif->reference_number  = $update->ld_id;
                $tarif->nomor_surat       = $update->reference_number;
                $tarif->origin            = "logpond";
                $tarif->destination       = "INDUSTRI, " . strtoupper($update->destination_name);
                $tarif->status            = "belum_dibayar";
                $tarif->status_pengiriman = "diterima";
                $tarif->shipped_date      = date("Y-m-d H:i:s");
                $tarif->arrived_date      = date("Y-m-d H:i:s");
                $tarif->ammount           = 0;
                $tarif->created_at        = $this->date;
                $tarif->created_by        = Auth::user()->id;
                $tarif->save();
                $data["status"]  = true;
                $data["message"] = "pengiriman berhasil.";
                $data["result"]  = $update;
                $data["kode"]    = 200;
            } else {
                $data["status"]  = false;
                $data["message"] = "gagal kirim.";
                $data["result"]  = [];
                $data["kode"]    = 401;
            }

        }

        return $data;
    }
}