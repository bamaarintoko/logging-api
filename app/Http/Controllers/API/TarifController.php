<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 3/5/2019
 * Time: 8:26 AM
 */

namespace App\Http\Controllers\API;


use App\Http\Models\Balance;
use App\Http\Models\LogpondDeliveryOrder;
use App\Http\Models\LogpondOut;
use App\Http\Models\Mutasi;
use App\Http\Models\Tarif;
use App\Http\Models\TpkDeliveryOrder;
use App\Http\Models\TpkOutStock;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TarifController
{
    private $date, $user;

    public function __construct()
    {
        $this->date = date("Y-m-d H:i:s");
        $this->user = Auth::user();
    }

    public function update_tarif(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_ammount' => 'required',
            'par_id' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 404;
            return $data;
        }
        $tarif             = Tarif::where("id", $input["par_id"])->first();
        $tarif->ammount    = $input["par_ammount"];
        $tarif->created_by = Auth::user()->id;
        $tarif->updated_at = $this->date;
        if ($tarif->update()) {
            $data["status"]  = true;
            $data["message"] = "konfirmasi pembayaran berhasil";
            $data["result"]  = $tarif;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "konfirmasi pembayaran gagal.";
            $data["result"]  = $tarif;
            $data["kode"]    = 404;
        }
        return $data;
    }

    public function get_history(Request $request)
    {

    }

    public function get_list_tarif(Request $request)
    {
        $tarif = Tarif::select("users.name", "tarif.*")->where("status_pengiriman", "diterima")->where("tarif.status", "belum_dibayar")->join("users", "users.id", "tarif.created_by")->orderBy('created_at', 'desc')->get();

        if (count($tarif) > 0) {
            $data["status"]  = true;
            $data["message"] = "data tersedia.";
            $data["result"]  = $tarif;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "tidak ada data.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        }
        return $data;
    }

    public function get_list_tarif_paid(Request $request)
    {
        $tarif = Tarif::select("users.name", "tarif.*")->where("status_pengiriman", "diterima")->where("tarif.status", "terbayar")->with(["paid_user"])->join("users", "users.id", "tarif.created_by")->get();

        if (count($tarif) > 0) {
            $data["status"]  = true;
            $data["message"] = "data tersedia.";
            $data["result"]  = $tarif;
            $data["kode"]    = 200;
        } else {
            $data["status"]  = false;
            $data["message"] = "tidak ada data.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        }
        return $data;
    }

    //API untuk mnampilkan detail pembayaran, daftar kayu yang dikirim, dan perkiraan harga
    public function get_detail_pembayaran(Request $request)
    {
        //                return "eqqqwewe";
        $input     = $request->all();
        $validator = Validator::make($input, ['par_type' => 'required',
            'par_tarif_id' => 'required']);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameter.";
            $data["result"]  = [];
            $data["kode"]    = 404;
            return $data;
        } else {
            $data  = array();
            $tarif = Tarif::with(["paid_user"])->where("id", $input["par_tarif_id"])->where("origin", $input["par_type"])->first();
            //                        return $tarif;
            if ($tarif) {
                $data["status"]          = true;
                $data["message"]         = "SUCCESS";
                $data["kode"]            = 200;
                $data["result"]["tarif"] = $tarif;
                if ($tarif->origin == "tpk") {
                    $data_ = TpkDeliveryOrder::where("tpk_delivery_order.id", $tarif->reference_number)->first();

                    if ($data_->destination == "industri") {
                        $data["result"]["payload"] = TpkDeliveryOrder::with(["created_user",
                            "accepted_user",
                            "out_item"])->where("tpk_delivery_order.id", $tarif->reference_number)->first();
                        $data["result"]["harga"]   = TpkOutStock::where('reference_number', $tarif->reference_number)->get();
                    } else {
                        $data["result"]["payload"] = TpkDeliveryOrder::with(["created_user",
                            "accepted_user",
                            "stock_item"])->where("tpk_delivery_order.id", $tarif->reference_number)->first();
                    }

                    //array perkiraan harga tpk
                    $data["result"]["harga"] = TpkOutStock::select(DB::raw('tpk_out_stock.*,kategori_kayu.*,price*volume as harga'))->where('reference_number', $tarif->reference_number)->join("kategori_kayu", "kategori_kayu.id", "tpk_out_stock.jenis_kayu")->get();
                    $data["result"]["total"] = TpkOutStock::select(DB::raw('SUM(price*volume) as total, SUM(price) as sub_total'))->where('reference_number', $tarif->reference_number)->join("kategori_kayu", "kategori_kayu.id", "tpk_out_stock.jenis_kayu")->get();
                } elseif ($tarif->origin == "logpond") {
                    $data["result"]["payload"] = LogpondDeliveryOrder::with(["created_user",
                        "accepted_user",
                        'out_item'])->where("logpond_delivery_order.ld_id", $tarif->reference_number)->first();

                    //array perkiraan harga logpond
                    $data["result"]["harga"] = LogpondOut::select(DB::raw('logpond_out.*,kategori_kayu.*,price*volume as 
                    harga'))->where('reference_number', $tarif->reference_number)->join("kategori_kayu", "kategori_kayu.id", "logpond_out.jenis_kayu")->get();
                    $data["result"]["total"] = LogpondOut::select(DB::raw('SUM(price*volume) as total, SUM(price) as sub_total'))->where('reference_number', $tarif->reference_number)->join("kategori_kayu", "kategori_kayu.id", "logpond_out.jenis_kayu")->get();
                }
            } else {
                $data["status"]  = false;
                $data["message"] = "error.";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
            return $data;
        }
        //        $detail = Tarif::where
    }

    //    API untuk melakukan konfirmasi pembayaran
    public function update_pembayaran(Request $request)
    {

        //        return $balance_aktif[0]->saldo_aktif;
        $input     = $request->all();
        $validator = Validator::make($input, ['par_id_pembayaran' => 'required',
            'par_invoice' => 'required',
            'par_jumlah' => "required"]);
        if ($validator->fails()) {
            $data["status"]  = false;
            $data["message"] = "missing parameters.";
            $data["result"]  = [];
            $data["kode"]    = 404;
        } else {
            $update                = Tarif::where("id", $input["par_id_pembayaran"])->first();
            $update->nomor_invoice = $input["par_invoice"];
            $update->ammount       = $input["par_jumlah"];
            $update->status        = "terbayar";
            $update->paid_by       = Auth::user()->id;
            $update->updated_at    = $this->date;
            $id                    = User::select("player_id")->where("id", $update->created_by)->first();
            $player_id             = array($id->player_id);

            $balance_aktif = DB::select("SELECT m.db - m.cr as saldo_aktif FROM (SELECT SUM(case when type ='kredit' then amount else 0 end) as cr, SUM(case when type ='debit' then 
amount else 0 end) as db
from mutasi) as m");

            //            cek saldo apakah mencukupi atau tidak
            if ($balance_aktif[0]->saldo_aktif < $update->ammount) {
                return message(false, "saldo tidak mencukupi.", [], 404);
            }

            if ($update->update()) {
                $mutasi               = new Mutasi();
                $mutasi->amount       = $update->ammount;
                $mutasi->note         = "pembayaran invoice " . $update->nomor_invoice;
                $mutasi->type         = "kredit";
                $mutasi->created_by   = Auth::user()->id;
                $mutasi->created_date = $this->date;
                $mutasi->save();
                send_notif("Pembayaran " . $update->nomor_surat, "Pengiriman " . $update->nomor_surat . " telah dibayar", $player_id);
                $data = message(true, "pembayaran berhasil", $update, 200);
            } else {
                $data["status"]  = false;
                $data["message"] = "pembayaran gagal.";
                $data["result"]  = [];
                $data["kode"]    = 404;
            }
            //            $data = $update;
        }
        return $data;
    }

    //    API untuk menambahkan balance
    public function add_balance(Request $request)
    {
        //        return "qwe";

        $input = $request->all();
        //        return $input;
        $validator = Validator::make($input, ['par_amount' => 'required|integer']);
        //        return $uploadedFile;
        if ($validator->fails()) {
            return message(false, $validator->messages()->all(), [], 404);
        }
        //        cek saldo apakah masih ada data dengan status pending
        $cek = Balance::where("status", "pending")->first();
        //        jika ada maka tambah saldo gagal
        if ($cek) {
            $data = message(false, "Tambah saldo gagal. Masih ada saldo dengan status pending.", [], 404);
            //            jika tidak ada maka lakukan proses tambah saldo
        } else {
            //            cek gambar
            if (!$request->hasFile('par_image')) {
                return message(false, "tidak ada gambar.", [], 404);
            }
            $size         = pow(1024, 3);
            $uploadedFile = $request->file('par_image');
            if ($uploadedFile->getSize() > $size) {
                return message(false, "ukuran maksimal 1 mb.", [], 404);
            }
            $ext = array("jpg",
                "jpeg",
                "png");

            //            validasi gambar
            if (!in_array(strtolower($uploadedFile->getClientOriginalExtension()), $ext)) {
                return message(false, "gagal upload gambar", [], 200);
            } else {
                $moveTo = public_path() . '/img';
                $name   = time() . "." . strtolower($uploadedFile->getClientOriginalExtension());
                $uploadedFile->move($moveTo, $name);
                //                $path = $uploadedFile->store('public/images');
            }
            $balance               = new Balance();
            $balance->amount       = $input['par_amount'];
            $balance->image        = $name;
            $balance->note         = $input['par_note'];
            $balance->status       = 'pending';
            $balance->created_date = $this->date;
            $balance->created_by   = Auth::user()->id;
            if ($balance->save()) {
                $data = message(true, "saldo berhasil ditambahkan dengan status pending", $balance, 200);
            } else {
                $data = message(false, 'saldo gagal ditambahkan', [], 404);
            }
        }
        return $data;

    }

    //    API untuk accept saldo yang pending
    public function accept_balance(Request $request)
    {
        $balance = Balance::where("status", "pending")->first();
        if ($balance) {
            //            return Auth::user()->id;
            $balance->status        = "success";
            $balance->accepted_by   = Auth::user()->id;
            $balance->accepted_date = $this->date;
            if ($balance->update()) {
                $mutasi               = new Mutasi();
                $mutasi->amount       = $balance->amount;
                $mutasi->note         = "penambahan saldo";
                $mutasi->type         = "debit";
                $mutasi->created_by   = Auth::user()->id;
                $mutasi->created_date = $this->date;

                if ($mutasi->save()) {
                    $data = message(true, "saldo berhasil di update", [], 200);
                } else {
                    $data = message(false, "saldo gagal diupdate", [], 404);
                }
            } else {
                $data = message(false, "saldo gagal diupdate", [], 404);
            }
        } else {
            $data = message(false, 'tidak ada saldo pending', [], 404);
        }
        return $data;
    }

    //    API untuk menampilkan mutasi
    public function get_mutasi(Request $request)
    {
        $input = $request->all();

        if ($input['filter_bulan']) {
            $month = $input['filter_bulan'];
        } else {
            $month = date('m');
        }

        if ($input['filter_bulan']) {
            $year = $input['filter_tahun'];
        } else {
            $year = date('Y');
        }


        $mutasi = DB::select("SELECT s.*,s.db - s.cr as bal,
@RunningBalance:= @RunningBalance +s.db - s.cr rb FROM (
    select mutasi_id,note,created_date,
    sum(case when type = 'debit' then amount else '-' end) as db,
    sum(case when type = 'kredit' then amount else '-' end) as cr
    FROM mutasi where MONTH(created_date) = '" . $month . "' AND YEAR(created_date) = '" . $year . "' GROUP BY mutasi_id) s,(Select @RunningBalance:=0) rb");
        //        dd(DB::getQueryLog());

        if (count($mutasi) > 0) {
            $data = message(true, "mutasi tersedia", $mutasi, 200);
        } else {
            $data = message(false, "tidak ada data mutasi", 200);
        }
        return $data;
    }

    //    API ambil range tahun dan bulan
    public function get_date()
    {
        //        $years = array_combine(range(date("Y"), 2000), range(date("Y"), 2000));
        $months = date("m");

        //        ammbil tahun pertama mulai mutasi
        $get_year_mutasi = Mutasi::orderBy("created_date", "asc")->first();

        //        cek tahun
        if ($get_year_mutasi) {
            $year = date('Y', strtotime($get_year_mutasi->created_date));
        } else {
            $year = date('Y');
        }

        //        range tahun
        $years         = range(date("Y"), $year);
        $data["month"] = $months;
        for ($x = 0; $x < count($years); $x++) {
            $data["year"][$x]["label"] = strval($years[$x]);
            $data["year"][$x]["value"] = strval($years[$x]);
        }
        //        return $months;
        return message(true, "ok", $data, 200);
    }

    //    API menampilkan saldo aktif dan pending
    public function get_saldo()
    {
        //        saldo pending
        $balance = Balance::where('status', '!=', "success")->sum('amount');

        //        saldo aktif
        $balance_aktif = DB::select("SELECT m.db - m.cr as saldo_aktif FROM (SELECT SUM(case when type ='kredit' then amount else 0 end) as cr, SUM(case when type ='debit' then 
amount else 0 end) as db
from mutasi) as m");

        $data = array("saldo_pending" => $balance,
            "saldo_aktif" => intval($balance_aktif[0]->saldo_aktif));
        return message(true, "balance", $data, 200);
    }

    //    API detail pending saldo
    public function get_pending_saldo()
    {
        $pending = Balance::with(["created_user"])->where("status", "pending")->first();
        if ($pending) {
            $pending->image = "http://siska.mlskoding.com/public/img/" . $pending->image;
            return message(true, "detail saldo", $pending, 200);
        } else {
            return message(false, "tidak ada saldo pending", [], 404);

        }
    }

    //    API riwayat semua saldo
    public function get_history_saldo()
    {
        $saldo = Balance::orderBy("created_date", "DESC")->get();
        if (count($saldo) > 0) {
            return message(true, "daftar saldo tersedia", $saldo, 404);
        } else {
            return message(false, "tidak ada data", $saldo, 404);
        }
    }

    //    CRONJOB TUTUP BUKU
    public function cron_job()
    {
        $date = "2019-05-03 22:51:39";
        //        date("Y-m-d H:i:s",strtotime($date."-1 minutes"));
        //        return date('Y-m-d H:i:s',strtotime("-1 minutes"));
        $balance_aktif = DB::select("SELECT m.db - m.cr as saldo_aktif FROM (SELECT SUM(case when type ='kredit' then amount else 0 end) as cr, SUM(case when type ='debit' then 
amount else 0 end) as db
from mutasi) as m");
        $balance       = $balance_aktif[0]->saldo_aktif;
        return $balance;
        if ($balance > 0) {
            $mutasi               = new Mutasi();
            $mutasi->amount       = $balance;
            $mutasi->note         = "cut off by system";
            $mutasi->type         = "kredit";
            $mutasi->created_by   = 9;
            $mutasi->created_date = $this->date;
            $mutasi->save();
        }
        //        return $balance_aktif;
    }

}