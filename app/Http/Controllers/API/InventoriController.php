<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 4/1/2019
 * Time: 7:42 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Models\IncomingInventory;
use App\Http\Models\InventoriOut;
use App\Http\Models\MasterInventory;
use App\Http\Models\Pdps;
use App\Http\Models\Spm;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InventoriController extends Controller
{
    private $date;

    //<Spm>
    public function __construct()
    {
        $this->date = date("Y-m-d H:i:s");
    }

    public function fail_message()
    {
        return $data = array("status" => false,
            "message" => "cek your parameter",
            "kode" => 401,
            "result" => []);
    }

    static function message($status = true, $message = 'message', $result = [], $kode = 200)
    {
        return $data = array("status" => $status,
            "message" => $message,
            "kode" => $kode,
            "result" => $result);
    }

    public function cek_spm()
    {
        //        return "qwe";
        $cek = Spm::with(['item'])->where('status', 'open')->first();

        if ($cek) {
            return self::message(true, "ada spm dengan status open.", $cek, 200);
            //            $data = array("status" => true,
            //                "message" => "ada spm dengan status open.",
            //                "kode" => 200,
            //                "result" => $cek);
        } else {
            return self::message(true, "tidak ada spm dengan status open.", [], 404);
            //            $data = array("status" => false,
            //                "message" => "tidak ada spm dengan status open.",
            //                "kode" => 404,
            //                "result" => []);
        }

        //        return $data;
    }

    public function create_spm()
    {
        $cek   = Spm::where('status', 'open')->first();
        $count = Spm::count();
        //        return $count+1000;
        $spm               = new Spm();
        $spm->nomor_surat  = $count + 1000;
        $spm->note         = 'qwe';
        $spm->created_date = $this->date;
        $spm->created_by   = Auth::user()->id;
        $spm->status       = 'open';
        //        $spm->save();
        if ($spm->save()) {
            $data = self::message(true, 'surat permintaan berhasil dibuat', $spm, 200);
            //            $data = array("status" => true,
            //                "message" => "surat permintaan berhasil dibuat.",
            //                "kode" => 200,
            //                "result" => []);
        } else {
            $data = self::message(false, 'surat permintaan gagal dibuat', [], 404);

            //            $data = array("status" => false,
            //                "message" => "surat permintaan gagal dibuat.",
            //                "kode" => 404,
            //                "result" => []);
        }

        return $data;
    }

    public function add_out_item(Request $request)
    {
        $input = $request->all();
        //        return "qwe";
        $validator = Validator::make($request->all(), ['par_spm_id' => 'required',
            'par_item_id' => 'required|unique:inventory_out,item_id',
            'par_qty' => 'required|numeric']);
        if ($validator->fails()) {
            //            $data = array("status" => false,
            //                "message" => "cek your parameter",
            //                "kode" => 401,
            //                "result" => []);
            return $this->fail_message();
        }
        $io           = new InventoriOut();
        $io->spm_id   = $input['par_spm_id'];
        $io->item_id  = $input['par_item_id'];
        $io->quantity = $input['par_qty'];
        //        return "qwe";
        $io->status       = 'process';
        $io->note         = $input['par_note'];
        $io->created_by   = Auth::user()->id;
        $io->created_date = $this->date;

        if ($io->save()) {
            $data = array("status" => true,
                "message" => "item berhasil ditambahkan",
                "kode" => 200,
                "result" => $io);
        } else {
            $data = array("status" => false,
                "message" => "item gagal ditambahkan",
                "kode" => 404,
                "result" => []);
        }

        return $data;
    }


    public function delete_out_item(Request $request)
    {
        //        return self::message(false,"error",[],500);
        $input = $request->all();
        //        return "qwe";
        $validator = Validator::make($request->all(), ['par_id' => 'required|numeric']);
        if ($validator->fails()) {
            return $this->fail_message();
        }
        $item = InventoriOut::where('id', $input['par_id'])->first();
        if ($item) {
            if ($item->delete()) {
                return self::message(true, 'item berhasil dihapus', [], 200);
            } else {
                return self::message(false, 'item gala dihapus', [], 404);
            }
        } else {
            return self::message(false, 'item gagal dihapus', [], 404);
        }
    }

    public function select_item(Request $request)
    {

        $input = $request->all();
        //        return $input;
        if ($input['keyword']) {
            $inventory = MasterInventory::where('nama_barang', 'like', "%{$input['keyword']}%")->get();
            //            return 'keyword';
        } else {
            $inventory = MasterInventory::limit(10)->get();
            //            return 'no keyword';
        }
        if (count($inventory) > 0) {
            $data['status']  = true;
            $data['message'] = 'pencarian ditemukan';
            $data['result']  = $inventory;
            $data['kode']    = 200;
        } else {
            $data['status']  = false;
            $data['message'] = 'pencarian tidak ditemukan';
            $data['result']  = [];
            $data['kode']    = 404;
        }
        return $data;
    }

    public function get_detail_spm(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_spm_id' => 'required']);
        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "cek your parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }
        $spm = Spm::with(['item'])->where('id', $input['par_spm_id'])->first();

        if ($spm) {
            $data['status']  = true;
            $data['message'] = 'spm tersedia';
            $data['kode']    = 200;
            $data['result']  = $spm;
        } else {
            $data['status']  = false;
            $data['message'] = 'spm tidak tersedia';
            $data['kode']    = 404;
            $data['result']  = [];
        }
        return $data;
    }

    public function update_out_item(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($request->all(), ['par_id' => 'required',
            'par_qty_accepted' => 'required',
            'par_status' => 'required']);
        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "cek your parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }

        $io                    = InventoriOut::where('id', $input['par_id'])->first();
        $io->quantity_accepted = $input['par_qty_accepted'];
        $io->note              = $input['par_note'];
        $io->status            = $input['par_status'];
        $io->created_by        = Auth::user()->id;
        $io->updated_at        = $this->date;
        if ($io->update()) {
            $data = array("status" => true,
                "message" => "item berhasil di update.",
                "kode" => 200,
                "result" => $io);
        } else {
            $data = array("status" => false,
                "message" => "item gagal di update.",
                "kode" => 404,
                "result" => $io);
        }

        return $data;

    }
    //    <Spm/>

    //    <Pdps>
    public function get_list_pdps(Request $request)
    {
        //        return "qwe";
        $pdps = Pdps::where('status', '!=', 'process')->get();
        //        return $pdps;
        if (count($pdps) > 0) {
            return self::message(true, 'data tersedia', $pdps, 200);
        } else {
            return self::message(false, 'data tidak tersedia', [], 404);
        }
    }

    public function cek_pdps(Request $request)
    {
        $cek = Pdps::with(['item'])->where('status', 'process')->first();
        if ($cek) {
            $data = array("status" => true,
                "message" => "masih ada pengajuan dana pembayaran suplier dengan status procee",
                "result" => $cek,
                "kode" => 200);
        } else {
            //        return "qwe";
            $data = array("status" => false,
                "message" => "tidak ada data dengan status process",
                "result" => [],
                "kode" => 404);
        }
        return $data;
    }

    public function create_pdps(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_invoice_number' => 'required|unique:pdps,invoice_number_mt']);

        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "cek your parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }

        $count_pdps              = Pdps::count();
        $pdps                    = new Pdps();
        $pdps->invoice_number_at = $count_pdps + 1000;
        $pdps->invoice_number_mt = $input['par_invoice_number'];
        $pdps->note              = $input['par_note'];
        $pdps->status            = 'process';
        $pdps->created_by        = Auth::user()->id;
        //        return $count_pdps;
        $pdps->created_date = $this->date;

        if ($pdps->save()) {
            $data = array("status" => true,
                "message" => "pengajuan dana pembayaran suplier berhasil dibuat",
                "kode" => 200,
                "result" => $pdps);
        } else {
            $data = array("status" => false,
                "message" => "pengajuan dana pembayaran suplier gagal dibuat",
                "kode" => 404,
                "result" => []);
        }
        return $data;
    }

    public function update_pdps(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_pdps_id' => 'required|numeric']);
        if ($validator->fails()) {
            return $this->fail_message();
        }

        $pdps         = Pdps::where('id', $input['par_pdps_id'])->first();
        $pdps->status = 'request';
        if ($pdps->update()) {
            return self::message(true, "pengajuan dana berhasil dibuat", $pdps, 200);
        } else {
            return self::message(false, 'pengajuan dana gagal dibuat', $pdps, 404);
        }
    }

    public function add_in_item(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_pdps' => 'required',
            'par_item_id' => 'required|numeric',
            'par_qty' => 'required|numeric']);

        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "cek your parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }

        $incoming               = new IncomingInventory();
        $incoming->pdps_id      = $input['par_pdps'];
        $incoming->item_id      = $input['par_item_id'];
        $incoming->quantity     = $input['par_qty'];
        $incoming->buy_price    = $input['par_price'];
        $incoming->note         = $input['par_note'];
        $incoming->created_by   = Auth::user()->id;
        $incoming->created_date = $this->date;

        if ($incoming->save()) {
            $data = array("status" => true,
                "message" => "tambah item berhasil",
                "kode" => 200,
                "result" => $incoming);
        } else {
            $data = array("status" => true,
                "message" => "tambah item gagal",
                "kode" => 404,
                "result" => []);
        }
        return $data;
    }

    public function delete_in_item(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['par_id' => 'required|numeric']);

        if ($validator->fails()) {
            $data = array("status" => false,
                "message" => "cek your parameter",
                "kode" => 401,
                "result" => []);
            return $data;
        }
        $item = IncomingInventory::where('id', $input['par_id'])->first();
        if ($item) {
            if ($item->delete()) {
                $data = array("status" => true,
                    "message" => "item berhasil dihapus",
                    "kode" => 200,
                    "result" => []);
            } else {
                $data = array("status" => false,
                    "message" => "item gagal dihapus",
                    "kode" => 404,
                    "result" => []);
            }
        } else {
            $data = array("status" => false,
                "message" => "item gagal dihapus",
                "kode" => 404,
                "result" => []);
        }
        return $data;

    }
    //    <Pdps/>

    //    <Pdo>

    //    <Pdo/>

    //    <Stock>
    public function get_stock(){
        $pdps = Pdps::selectRaw('sum(incoming_inventory.quantity) as qty, master_inventory.*')->where
        ('status','paid')
            ->join('incoming_inventory','pdps.id','incoming_inventory.pdps_id')
            ->join('master_inventory','incoming_inventory.item_id','master_inventory.id')
            ->groupBy('incoming_inventory.item_id')
            ->get();
        return $pdps;
    }
    //    <Stock/>
}