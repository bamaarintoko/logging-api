<?php

namespace App\Http\Middleware;

use App\Http\Models\Settings;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //        $next($request);
        //        die("qwe");
        if (Auth::user()->status === "not-active") {
            $data["status"]  = false;
            $data["message"] = "You are currently blocked by administrator.";
            $data["result"]  = [];
            $data["kode"]    = 666;
            return response()->json($data);
        } else {
            $cek = Settings::get();
            if ($cek[0]->value === "1") {
                $data["status"]  = false;
                $data["message"] = $cek[2]->value;
                $data["result"]  = [];
                $data["kode"]    = 666;
                return response()->json($data);

            }
            //            die("qwe");
            return $next($request);

        }
        //        return $next($request);
    }
}
