<?php

namespace App\Console;

use App\Http\Models\Balance;
use App\Http\Models\Mutasi;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [//
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
//        return date()
//        $balance_aktif = DB::select("SELECT m.db - m.cr as saldo_aktif FROM (SELECT SUM(case when type ='kredit' then amount else 0 end) as cr, SUM(case when type ='debit' then
//amount else 0 end) as db
//from mutasi) as m");
//        $balance       = $balance_aktif[0]->saldo_aktif;
        $balance       = 100000;
        if ($balance > 0) {
            $mutasi               = new Mutasi();
            $mutasi->amount       = $balance;
            $mutasi->note         = "cut off by system";
            $mutasi->type         = "kredit";
            $mutasi->created_by   = 9;
            $mutasi->created_date = date('Y-m-d H:i:s',strtotime("-1 minutes"));
            $mutasi->save();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
